/* Copyright 1995 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* main.c
 *
 * Author: Keith Ruttle (Acorn)
 *
 * Description
 * ===========
 * SetGtWay utility main code
 *
 * Environment
 * ===========
 * Acorn RISC OS 3.11 or later.
 *
 * Compiler
 * ========
 * Acorn Archimedes C release 5.06 or later.
 *
 * Change record
 * =============
 *
 * JPD  Jem Davies (Cambridge Systems Design)
 *
 *
 * 10-Oct-95  15:46  JPD  Version 1.00
 * First version with change record: make compile without warnings.
 *
 *
 **End of change record*
 */

#include <string.h>
#include <stdlib.h>

#include "kernel.h"
#include "swis.h"

#include "main.h"
#include "setgtway.h"

/******************************************************************************/

static void setvar(char *var, char *cmd)
{
/*
 * Set an OS variable
 */

   _kernel_swi_regs r;

   r.r[0] = (int)var;
   r.r[1] = (int)cmd;
   r.r[2] = strlen(cmd)+1;
   r.r[3] = 0;
   r.r[4] = 0;
   (void) _kernel_swi(XOS_Bit | OS_SetVarVal, &r, &r);

   return;

} /* setvar() */

/******************************************************************************/

void gw_error(char *str)
{
/*
 * On an error, set the appropriate system variable, then exit
 */

   if (*str)
      setvar("BootNet$Error", str);

   release_msgs();

   exit(1);

} /* gw_error() */

/******************************************************************************/

int main(int argc, char **argv)
{

   argc = argc;
   argv = argv;

   (void) setgateway();

   exit(0);

} /* main() */

/******************************************************************************/

/* EOF main.c */
