/* Copyright 1995 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* setgtway.c
 *
 * Author: Keith Ruttle (Acorn)
 *
 * Description
 * ===========
 * SetGtWay utility code
 *
 * Environment
 * ===========
 * Acorn RISC OS 3.11 or later.
 *
 * Compiler
 * ========
 * Acorn Archimedes C release 5.06 or later.
 *
 * Change record
 * =============
 *
 * JPD  Jem Davies (Cambridge Systems Design)
 *
 *
 * 13-Nov-95  16:19  JPD  Version 1.00
 * First version with change record: compiles without warnings with new
 * Internet include structure and DCI-4 structs
 *
 *
 **End of change record*
 */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "kernel.h"
#include "swis.h"

#include "sys/types.h"
#include "sys/socket.h"
#include "sys/ioctl.h"
#include "net/if.h"
#include "netinet/in.h"
#include "sys/dcistructs.h"

#include "inetlib.h"
#include "socklib.h"

#include "main.h"
#include "setgtway.h"

#define MAP_FILE    "<Gateway$Dir>.Files.MAP"
#define CONFIG_FILE "<Gateway$Dir>.Files.Configure"

#define SITE_DEFAULT  1

/*
 * field offsets in AUN format IP address
 */
#define SITE      0
#define NETWORK   1
#define NET       2
#define STATION   3

#define MIN_IFCNT 2

/* MAX_IFCNT used to be set to 5, which I believe was an error and should
 * have been 4. One could consider setting it to 8/9 for Risc-PCs with many
 * slices, but I believe that, in practice, it should not be greater than 4
 * as NetG will not cope. Defined behaviour for !Gateway is only up to two
 * networks anyway, so maybe it should be set to 2? Currently, leave it
 * unchanged.
 */
#define MAX_IFCNT 5

struct ifinfo
{
   struct ifinfo *if_next;
   char   if_sitename[32];
   char   if_netname[32];
   char   if_device[12];
   u_char if_siteid;
   u_char if_networkid;
   u_char if_netid;
   u_char if_stationid;
   int    if_unit;
   int    if_slot;
};

#define Err_Sta      0
#define Err_OpenConf 1
#define Err_SlotNbr  2
#define Err_SiteName 3
#define Err_Device   4
#define Err_IfCnt    5
#define Err_NetName  6
#define Err_DupEco   7
#define Err_DupSlot  8
#define Err_NetNbr   9
#define Err_OpenMap  10
#define Err_NetRange 11
#define Err_CLI      12
#define Err_Inet     13
#define Err_IfName   14
#define Err_IfMask   15
#define Err_IfAddr   16

static char *tokens[17] = {
    "GtwSta",
    "GtwOpC",
    "GtwSon",
    "GtwSid",
    "GtwDev",
    "GtwIfn",
    "GtwNam",
    "GtwEcd",
    "GtwSod",
    "GtwNtl",
    "GtwOpM",
    "GtwNor",
    "GtwCli",
    "GtwInt",
    "GtwIfN",
    "GtwIfM",
    "GtwIfA",
};

#define MSG_FILE "Resources:$.Resources.Net.Messages"

static struct ifinfo *iflist;
static u_char local_station_id, local_net_id, net_id_matched;
static char msg_fd[16];
static char textbuf[64];
static int podulenumber;

static _kernel_oserror *init_msgs(char *filename);
static void read_local_net(void);
static void read_podule_number(void);
static void read_local_station(void);
static void read_server_config(char *config_file);
static int map_slot_to_name(char *cp, int slotno);
static void check_configure(void);
static void configure_interfaces(void);
static void create_address_map(char *map_file);
static char *skip(char **cpp);
static char *nextline(FILE *fd, char *line, int len);
static void do_cli(char *str);
static void ifconfig(char *name, u_long addr);
static int caseless_strcmp(char *a, char *b) ;
static int caseless_strncmp(char *a, char *b, int n) ;
static void ifpattach(struct ifinfo *q);

/******************************************************************************/

void setgateway(void)
{
   _kernel_oserror *e;

   local_station_id = 0;
   local_net_id = 0;
   net_id_matched = 0;
   iflist = 0;
   e = init_msgs(MSG_FILE);

   /* If error, note it and exit */
   if (e)
      gw_error(e->errmess);

   read_podule_number();

   read_local_station();

   read_local_net();

   read_server_config(CONFIG_FILE);

   create_address_map(MAP_FILE);

   check_configure();

   configure_interfaces();

   release_msgs();

   return;

} /* setgateway() */

/******************************************************************************/

static _kernel_oserror *init_msgs(char *filename)
{
   _kernel_swi_regs r;
   _kernel_oserror *e;

   r.r[1] = (int)filename;
   e = _kernel_swi(XOS_Bit | MessageTrans_FileInfo, &r, &r);
   if (e)
       return e;

   r.r[0] = (int)msg_fd;
   r.r[1] = (int)filename;
   r.r[2] = 0;
   e = _kernel_swi(XOS_Bit | MessageTrans_OpenFile, &r, &r);

   return e;

} /* init_msgs() */

/******************************************************************************/

void release_msgs(void)
{
   _kernel_swi_regs r;

   r.r[0] = (int)msg_fd;
   (void) _kernel_swi(XOS_Bit | MessageTrans_CloseFile, &r, &r);

   return;

} /* release_msgs() */

/******************************************************************************/

static char *mns_str(int token)
{
   _kernel_swi_regs r;
   _kernel_oserror *e;

   memset ((char *)&r, 0, sizeof(r));
   memset (textbuf, 0, sizeof(textbuf));
   r.r[0] = (int)msg_fd;
   r.r[1] = (int)tokens[token];
   r.r[2] = (int)textbuf;
   r.r[3] = sizeof(textbuf);
   e = _kernel_swi(XOS_Bit | MessageTrans_Lookup, &r, &r);

   return (e ? tokens[token] : textbuf);

} /* mns_str() */

/******************************************************************************/

static void read_local_net(void)
{
   _kernel_oserror *e;
   _kernel_swi_regs r;

   e = _kernel_swi(XOS_Bit | Econet_ReadLocalStationAndNet, &r, &r);
   if (!e)
   {
      local_net_id = r.r[1];
      if (local_net_id == 0)
         net_id_matched = 1;
   }

   return;

} /* read_local_net() */

/******************************************************************************/

static void read_podule_number(void)
{
/*
 * Return total number of expansion cards (i.e. slots in machine)
 */

   _kernel_oserror *e;
   _kernel_swi_regs r;

   e = _kernel_swi(XOS_Bit | Podule_ReturnNumber, &r, &r);

   /* If any error assume 4 slots in this machine */
   podulenumber = e ? 4 : r.r[0];

   return;

} /* read_podule_number() */

/******************************************************************************/

#define READCMOS      161
#define StationID     0

static void read_local_station(void)
{
/*
 * Read local station number configured in CMOS RAM
 */
   _kernel_oserror *e;
   _kernel_swi_regs r;

   r.r[0] = READCMOS;
   r.r[1] = StationID;
   e = _kernel_swi(XOS_Bit | OS_Byte, &r, &r);

   if (!e)
      local_station_id = r.r[2];

   if (local_station_id <= 1)
      gw_error(mns_str(Err_Sta));

   return;

} /* read_local_station() */

/******************************************************************************/

static void read_server_config(char *config_file)
{
/*
 * Read configuration information from file
 */

   FILE *infof;
   struct ifinfo *ifp;
   char *cp, *cpp, site, eco, slot, other;
   char sitename[32], obuf[32];
   char line[256];
   int slotno;

   /* open file and exit on error */
   if ((infof = fopen(config_file, "r" )) == NULL)
      gw_error(mns_str(Err_OpenConf));

   sitename[0] = 0;

   for (;;)
   {
      site = eco = slot = other = 0;
      slotno = -1;

      /* read a line from the file, skip white space, comments etc. */
      while ((cpp = nextline(infof, line, 255), cpp != 0) &&
                                                 (*cpp == '|' || *cpp == '\0'));
         if (cpp == NULL)
         {
            fclose(infof);
            return;
         }

      cp = skip(&cpp);
#ifdef DO_SITE
      if (caseless_strcmp(cp, "site") == 0)
         site++;
      else
#endif
         /* if keyword is SLOT */
         if (caseless_strncmp(cp, "slot", 4) == 0)
         {
            slot++;
            cp = skip(&cpp);
            /* get slot number */
            slotno = atoi(cp);
            if (slotno < 0 || slotno >= podulenumber)
               gw_error(mns_str(Err_SlotNbr));
         }
         else
            if (caseless_strcmp(cp, "econet") == 0)
               eco++;
            else
            {
               strcpy(obuf, cp);
               other++;
            }
       cp = skip(&cpp);
       if (caseless_strcmp(cp, "is") == 0)
          cp = skip(&cpp);
#ifdef DO_SITE
       if (site)
       {
          if (sitename[0])
             gw_error(mns_str(Err_SiteName));
          strcpy(sitename, cp);
          continue;
       }
#endif
       /* get some space to contain the info for this slot */
       ifp = (struct ifinfo *)malloc(sizeof(struct ifinfo));

       if (ifp == NULL)
          gw_error("No memory");

       memset((char *)ifp, 0, sizeof(struct ifinfo));

       /* copy in details */
       ifp->if_siteid = SITE_DEFAULT;
       strcpy(ifp->if_netname, cp);
       if (eco)
       {
          strcpy(ifp->if_device, "ec");
          ifp->if_slot = -1;
       }
       if (other)
       {
          strcpy(ifp->if_device, obuf);
          ifp->if_device[2] = 0;
          ifp->if_slot = -1;
       }
       if (slot)
       {
          if (!map_slot_to_name(ifp->if_device, slotno))
             gw_error(mns_str(Err_Device));
          ifp->if_slot = slotno;
       }
       if (sitename[0])
          strcpy(ifp->if_sitename, sitename);

       /* add the info for this slot to the list */
       ifpattach(ifp);
   }

   return;

} /* read_server_config() */

/******************************************************************************/

static int map_slot_to_name(char *cp, int slotno)
{
/*
 * Find out which driver module controls the device in a given backplane slot
 *
 * Parameters:
 *    cp     : pointer to where to return the 2 byte text string name of
 *             physical interface type controlled by driver ("ea", "en", etc.)
 *    slotno : the slot number
 *
 * Returns:
 *    0 : => some error
 *   !0 : => all OK
 *
 */

   _kernel_swi_regs r;
   _kernel_oserror *e;
   struct dib *d;
   struct chaindib *chdp, *n;
   int res = 0;

   r.r[0] = 0;         /* initialise to zero so that we can detect a response */
   r.r[1] = Service_EnumerateNetworkDrivers;
   /* Issue service call to find all DCI4 drivers */
   e = _kernel_swi(XOS_Bit | OS_ServiceCall, &r, &r);


   /* if no error issuing service call and received a response */
   if ((e == 0) && (chdp = (struct chaindib *)(r.r[0]), chdp != 0))
   {
      n = chdp->chd_next;
      d = chdp->chd_dib;

      while (d != 0)
      {
         if (d->dib_slot.sl_slotid == slotno)
         {
            strcpy(cp, (char *)d->dib_name);
            res = 1;
            break;
         }
         else
         {
            if (n == 0)
               break;

            /* step on */
            d = n->chd_dib;
            n = n->chd_next;
         }
      }

      /* Now, free all the chaindibs returned to us */
      while (chdp != 0)
      {
         r.r[0] = 7;      /* reason code Free */
         r.r[2] = (int) chdp;
         chdp = chdp->chd_next;
         if (e = _kernel_swi(XOS_Bit | OS_Module, &r, &r), e != 0);
            /* if any error, probably should not continue freeing blocks */
            break;
      }
   }

   return res;

} /* map_slot_to_name() */

/******************************************************************************/

static void check_configure(void)
{
/*
 * check the configuration info
 */

   struct ifinfo *ifp, *ifpp;
   int unit, ifcnt = 0;

   for (ifp = iflist; ifp; ifp = ifp->if_next)
      ifcnt++;

   if (ifcnt < MIN_IFCNT || ifcnt > MAX_IFCNT)
      gw_error(mns_str(Err_IfCnt));

   for (ifp = iflist; ifp; ifp = ifp->if_next)
      if (ifp->if_networkid == 0)
         gw_error(mns_str(Err_NetName));

   for (ifp = iflist; ifp; ifp = ifp->if_next)
      if (caseless_strcmp(ifp->if_device, "ec") == 0 && !net_id_matched)
         gw_error("Connected Econet number not in MAP file");

   for (ifp = iflist; ifp; ifp = ifp->if_next)
   {
      if (ifp->if_unit > 0)
         continue;
      unit = 1;
      ifp->if_unit = unit++;
      for (ifpp = ifp->if_next; ifpp; ifpp = ifpp->if_next)
      {
         if (caseless_strcmp(ifp->if_device, ifpp->if_device) != 0)
            continue;
         if (caseless_strcmp(ifp->if_device, "ec") == 0)
            gw_error(mns_str(Err_DupEco));
         ifpp->if_unit = unit++;
      }
   }

   return;

} /* check_configure() */

/******************************************************************************/

static void configure_interfaces(void)
{
/*
 * Configure (ifconfig) the interfaces up
 */

   struct ifinfo *ifp;
   u_long ifaddr;
   char ifbuf[64], device[12];

   for (ifp = iflist; ifp; ifp = ifp->if_next)
   {
      if (strcmp(ifp->if_device, "ec") != 0)
      {
         if (ifp->if_netid < 128)
            gw_error(mns_str(Err_NetNbr));
      }
      else
         if (local_net_id != 0)
            ifp->if_netid = local_net_id;

      sprintf(ifbuf, "%d.%d.%d.%d",
                     ifp->if_siteid,
                     ifp->if_networkid,
                     ifp->if_netid,
                     ifp->if_stationid);
      ifaddr = inet_addr(ifbuf);
      sprintf(device, "%s%d", ifp->if_device, ifp->if_unit - 1);

      ifconfig(device, ifaddr);
   }

   do_cli("inetgateway 1");
   do_cli("addmap 0");

   return;

} /* configure_interfaces() */

/******************************************************************************/

static void create_address_map(char *map_file)
{
   FILE *infof;
   char *cp = 0, *cpp, line[256], cli_command[64];
   char cur_sitename[32], cur_netname[32];
   int cur_ip_address[4], net, first_read = 0;
   int cur_site_id, cur_network_id;
   struct ifinfo *ifp;

   /* open the map file */
   if ((infof = fopen(map_file, "r" )) == NULL)
      gw_error(mns_str(Err_OpenMap));

   cur_site_id = SITE_DEFAULT;
   cur_network_id = 0;
   cur_ip_address[SITE] = cur_site_id;
   cur_ip_address[STATION] = 0;
   cur_sitename[0] = 0;
   cur_netname[0] = 0;

   for (;;)
   {
      while ((cpp = nextline(infof, line, 255), cpp != 0) &&
                                                  (*cpp == '|' || *cpp == '\0'))
         ;

      if (cpp == NULL)
      {
         fclose(infof);
         return;
      }
#ifdef DO_SITE
      cp = skip(&cpp);
      if (*cpp == 0 && *cp && !isdigit(*cp))
      {
         cur_ip_address[SITE] = cur_site_id;
         strcpy(cur_sitename, cp);
         cur_network_id = 0;
         for (ifp = iflist; ifp; ifp = ifp->if_next)
         {
            if (caseless_strcmp(cp, ifp->if_sitename) == 0)
            {
               ifp->if_siteid = cur_site_id;
            }
         }
         cur_site_id++;
         continue;
      }
      first_read = 1;
#endif
      for (;;)
      {
         if (first_read)
            first_read = 0;
         else
            cp = skip(&cpp);

         if (*cp == '\0')
            break;

         if (isdigit(*cp))
         {
            net = atoi(cp);
            if (net == 0 || net > 252)
                gw_error(mns_str(Err_NetRange));
            if (net == local_net_id)
                net_id_matched++;
            cur_ip_address[NET] = net;
            sprintf(cli_command, "addmap 1 %d.%d.%d.%d %d",
                   cur_ip_address[SITE],
                   cur_ip_address[NETWORK],
                   cur_ip_address[NET],
                   cur_ip_address[STATION],
                   net);
            strcat(cli_command, " ");
            strcat(cli_command, cur_sitename[0] ? cur_sitename : "ThisSite");
            if (cur_netname[0])
            {
               strcat(cli_command, " ");
               strcat(cli_command, cur_netname);
            }

            do_cli(cli_command);

            for (ifp = iflist; ifp; ifp = ifp->if_next)
            {
               if (ifp->if_networkid == cur_ip_address[NETWORK])
               {
                  if (ifp->if_netid == 0)
                     ifp->if_netid = net;
                  ifp->if_stationid = local_station_id;
               }
            }
         }
         else
         {
            cur_ip_address[NETWORK] = ++cur_network_id;
            strcpy(cur_netname, cp);
            for (ifp = iflist; ifp; ifp = ifp->if_next)
            {
               if (caseless_strcmp(cp, ifp->if_netname) == 0)
               {
                  ifp->if_networkid = cur_ip_address[NETWORK];
               }
            }
         }
       }
    }

    return;

} /* create_address_map() */

/******************************************************************************/

static char *skip(char **cpp)
{
   register char *cp = *cpp;
   char *start;

   while (*cp == ' ' || *cp == '\t')
      cp++;

   if (*cp == '|')
      *cp = '\0';

   if (*cp == '\0')
   {
      *cpp = (char *)0;
      return cp;
   }

   start = cp;
   while (*cp && *cp != ' ' && *cp != '\t')
      cp++;
   if (*cp == '|')
      *cp = '\0';
   if (*cp != '\0')
      *cp++ = '\0';
   *cpp = cp;

   return start;

} /* skip () */

/******************************************************************************/

static char *nextline(FILE *fd, char *line, int len)
{
   char *cp;

   if (fgets(line, len, fd) == NULL)
      return (char *)0;
   cp = strchr(line, '\n');
   if (cp)
      *cp = '\0';

   return line;

} /* nextline() */

/******************************************************************************/

static void do_cli(char *str)
{
   _kernel_oserror *e;
   _kernel_swi_regs r;

   r.r[0] = (int)str;
   e = _kernel_swi(XOS_Bit | OS_CLI, &r, &r);
   if (e)
      gw_error(mns_str(Err_CLI));

   return;

} /* do_cli() */

/******************************************************************************/

static void ifconfig(char *name, u_long addr)
{
   struct sockaddr_in sinn;
   struct ifreq ifr;
   int s;

   strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name));

   s = socket(AF_INET, SOCK_DGRAM, 0);
   if (s < 0)
      gw_error(mns_str(Err_Inet));

   if (socketioctl(s, SIOCGIFFLAGS, (caddr_t)&ifr) < 0)
   {
      socketclose(s);
      gw_error(mns_str(Err_IfName));
   }

   if (socketioctl(s, SIOCGIFADDR, (caddr_t)&ifr) >= 0 &&
                ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr == addr)
      goto out;

   strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name));
   sinn.sin_family = AF_INET;
   sinn.sin_addr.s_addr = htonl(0xffff0000);
   ifr.ifr_addr = *(struct sockaddr *)&sinn;
   if (socketioctl(s, SIOCSIFNETMASK, (caddr_t)&ifr) < 0)
   {
      socketclose(s);
      gw_error(mns_str(Err_IfMask));
   }

   strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name));
   sinn.sin_family = AF_INET;
   sinn.sin_addr.s_addr = addr;
   ifr.ifr_addr = *(struct sockaddr *) &sinn;
   if (socketioctl(s, SIOCSIFADDR, (caddr_t)&ifr) < 0)
   {
      socketclose(s);
      gw_error(mns_str(Err_IfAddr));
   }

out:
   socketclose(s);
   return;

} /* ifconfig() */

/******************************************************************************/

static int caseless_strcmp(char *a, char *b)
{
   int d;

   while (*a || *b)
   {
      d = toupper(*(a++)) - toupper(*(b++));
      if (d)
         return d;
   }

   return 0;

} /* caseless_strcmp() */

/******************************************************************************/

static int caseless_strncmp(char *a, char *b, int n)
{
   int d;

   while ((*a || *b) && (n-- > 0))
   {
      d = toupper(*(a++)) - toupper(*(b++));
      if (d)
         return d;
   }

   return 0;

} /* caseless_strncmp() */

/******************************************************************************/

static void ifpattach(struct ifinfo *q)
{
   struct ifinfo **p;

   p = &iflist;
   while (*p)
   {
       p = &((*p)->if_next);
   }
   *p = q;

   return;

} /* ifpattach() */

/******************************************************************************/

/* EOF setgtway.c */
